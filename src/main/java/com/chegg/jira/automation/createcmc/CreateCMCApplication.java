package com.chegg.jira.automation.createcmc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreateCMCApplication {

    public static void main(String[] args) {
        SpringApplication.run(CreateCMCApplication.class, args);
    }

}
