package com.chegg.jira.automation.createcmc.client;

import org.springframework.beans.factory.annotation.Value;
import lombok.extern.slf4j.Slf4j;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
@Slf4j
public class BaseJiraClient implements IClient {

    @Value("${jira.server.url}")
    private String jira_url;

    @Value("${jira.server.username}")
    private String jira_username;

    @Value("${jira.server.password}")
    private String jira_password;

    JiraRestClientFactory jiraRestClientFactory;
    JiraRestClient jiraRestClient = null;

    @Override
    public JiraRestClient getJiraClientInstance() {
        try {
            URI uri = new URI(jira_url);
            jiraRestClientFactory = new AsynchronousJiraRestClientFactory();
            jiraRestClient = jiraRestClientFactory.createWithBasicHttpAuthentication(uri, jira_username, jira_password);
            log.info("Create JIRA Client object successfully {}", jiraRestClient.getMetadataClient().getServerInfo().get());
        }
        catch(Exception jiraClientException) {
            log.info("Unable to get JIRA client connection {}",jiraClientException.getMessage());
        }
        return jiraRestClient;
    }

}
