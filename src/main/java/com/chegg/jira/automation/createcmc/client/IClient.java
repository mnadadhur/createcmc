package com.chegg.jira.automation.createcmc.client;

import com.atlassian.jira.rest.client.api.JiraRestClient;

public interface IClient {
    JiraRestClient getJiraClientInstance();
}
