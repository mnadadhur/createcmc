package com.chegg.jira.automation.createcmc.command;

import com.chegg.jira.automation.createcmc.executor.CreateTicket;
import com.chegg.jira.automation.createcmc.executor.ValidateTicket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "create",
        mixinStandardHelpOptions = true,
        description = ": Command to create a new CMC ticket.")
@Component
@Slf4j
public class CreateTicketCommand implements Callable<Integer> {

    @CommandLine.Option(
            names = {"--p", "--project"},
            defaultValue = "unknown",
            description = "Provide project for creating the CMC ticket.",
            required = true)
    String jira_project;

    private CreateTicket createTicketExecutor;

    @Autowired
    public CreateTicketCommand(CreateTicket createTicketExecutor) {
        this.createTicketExecutor = createTicketExecutor;
    }

    @Override
    public Integer call() {
        String jiraTicketKey;
        try {
            log.info("Creating new CMC-ticket");
            jiraTicketKey = createTicketExecutor.createCMCTicket(jira_project);
            log.info("Jira ticket created: " + jiraTicketKey);
            return 0;
        } catch (Exception exception) {
            log.info("Unable to execute the specified JIRA command");
            return 1;
        }
    }
}
