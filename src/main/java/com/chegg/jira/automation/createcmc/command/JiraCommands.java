package com.chegg.jira.automation.createcmc.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.concurrent.Callable;

/**
 * Command for particular action type.
 */
@CommandLine.Command(
        name = "jira",
        mixinStandardHelpOptions = true,
        description = "Command line to validate Release checklist.",
        subcommands = {ValidateTicketCommand.class, CreateTicketCommand.class})
@Component
@Slf4j
public class JiraCommands implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        log.info("Please execute sub-command for specific action");
        return Integer.valueOf(-1);
    }
}
