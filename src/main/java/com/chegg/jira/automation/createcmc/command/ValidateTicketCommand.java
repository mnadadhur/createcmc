package com.chegg.jira.automation.createcmc.command;
import com.chegg.jira.automation.createcmc.executor.ValidateTicket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "validate",
        mixinStandardHelpOptions = true,
        description = ": Command to validate if a CMC has been created.")
@Component
@Slf4j
public class ValidateTicketCommand implements Callable<Integer> {


    @CommandLine.Option(
            names = {"--n", "--name"},
            defaultValue = "unknown",
            description = "Command action type",
            required = true)
    private String jira_key;

    private ValidateTicket validateTicketExecutor;

    @Autowired
    public ValidateTicketCommand(ValidateTicket validateTicketExecutor) {
        this.validateTicketExecutor = validateTicketExecutor;
    }

    @Override
    public Integer call() {
        try {
            log.info("Getting summary details for jira ticket : " + jira_key);
            validateTicketExecutor.getTicketDetails(jira_key);
            return 0;
        } catch (Exception exception) {
            log.info("Unable to execute the specified JIRA command");
            return 1;
        }
    }
}
