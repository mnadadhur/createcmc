package com.chegg.jira.automation.createcmc.executor;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.domain.*;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.LinkIssuesInput;
import com.chegg.jira.automation.createcmc.client.BaseJiraClient;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CreateTicket {

    private BaseJiraClient baseJiraClient;
    IssueField issueField = null;
    BasicIssue jira_key = null;

    @Autowired
    public CreateTicket(BaseJiraClient baseJiraClient) {
        this.baseJiraClient = baseJiraClient;
    }

    public String createCMCTicket(String jiraProject) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", "Learning Services");
            jsonObject.put("id", "13150");
            IssueRestClient issueRestClient = baseJiraClient.getJiraClientInstance().getIssueClient();
            IssueInputBuilder issueInputBuilder = new IssueInputBuilder();
            issueInputBuilder.setProjectKey(jiraProject);
            issueInputBuilder.setIssueTypeId(10401L);
            issueInputBuilder.setSummary("Release to production");
            issueInputBuilder.setDescription("h2. Release checklist:\n\n|Smoke Tests|(/)|\n|" +
                    "Load Tests|(/)|\n|" +
                    "ECS Restarts after secrets/param store changes|(/)|\n|Staging Health check post master build deployment|(/)|\n|" +
                    "Infrastructure changes completed in test|(/)|\n|" +
                    "DB schema changes|(/)|\n|" +
                    "Other config change|(/)|");
            issueInputBuilder.setFieldValue("customfield_12503", "Depending on the service the changes will be rolled back to the previous version.");
            List<String> issueLabels = new ArrayList<String>();
            issueLabels.add("release_cmc_09192021");
            issueInputBuilder.setFieldValue(IssueFieldId.LABELS_FIELD.id, issueLabels);
            issueInputBuilder.setFieldValue("customfield_12507", "Required Materials");
            issueInputBuilder.setFieldValue("customfield_12502", "2020-10-06T10:43:00.000-0700");
            issueInputBuilder.setFieldValue("customfield_14700", "https://jenkins.cheggnet.com/jenkins/view/Projects/view/UnitedTeamAutomation/view/Regression/view/RQM/job/RQM-Regression-Parent-BE-TEST/");
            issueInputBuilder.setFieldValue("customfield_14701", "https://jenkins.cheggnet.com/jenkins/view/Projects/view/UnitedTeamAutomation/view/Regression/view/RQM/job/RQM-Regression-Parent-Textbooks-PROD/");
            issueField = new IssueField("13150", "Learning Services", null, jsonObject);
            issueInputBuilder.setFieldValue("customfield_13610", issueField);
            jira_key = issueRestClient.createIssue(issueInputBuilder.build()).get();
            log.info("CMC ticket created {}", jira_key.getKey());
            LinkIssuesInput linkIssuesInput = new LinkIssuesInput("RQM-10984", jira_key.getKey(), "Relates");
            issueRestClient.linkIssue(linkIssuesInput);
        } catch (Exception ticketCreationError) {
            log.info("Unable to create a CMC ticket {}", ticketCreationError.getMessage());
        }
        return jira_key.getKey();
    }

}
