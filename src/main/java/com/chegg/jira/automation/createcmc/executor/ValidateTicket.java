package com.chegg.jira.automation.createcmc.executor;

import com.atlassian.jira.rest.client.api.domain.Issue;
import com.chegg.jira.automation.createcmc.client.BaseJiraClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ValidateTicket {

    private BaseJiraClient baseJiraClient;
    private Issue issue;

    @Autowired
    public ValidateTicket(BaseJiraClient baseJiraClient) {
        this.baseJiraClient = baseJiraClient;
    }

    public String getTicketDetails(String jira_key) {
        try {
            issue = baseJiraClient.getJiraClientInstance().getIssueClient().getIssue(jira_key).get();
            log.info(issue.getSummary());
        }
        catch (Exception ticketValidationError) {
            log.info("Unable to get ticket details {}", ticketValidationError.getMessage());
        }
        return issue.getSummary();
    }
}
